-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 28 fév. 2019 à 08:21
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `test_stage`
--

-- --------------------------------------------------------

--
-- Structure de la table `annees_scolaire`
--

DROP TABLE IF EXISTS `annees_scolaire`;
CREATE TABLE IF NOT EXISTS `annees_scolaire` (
  `id_annee_scolaire` int(11) NOT NULL AUTO_INCREMENT,
  `annee_scolaire` year(4) NOT NULL,
  PRIMARY KEY (`id_annee_scolaire`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `annees_scolaire`
--

INSERT INTO `annees_scolaire` (`id_annee_scolaire`, `annee_scolaire`) VALUES
(1, 2017);

-- --------------------------------------------------------

--
-- Structure de la table `classes`
--

DROP TABLE IF EXISTS `classes`;
CREATE TABLE IF NOT EXISTS `classes` (
  `id_classe` int(11) NOT NULL AUTO_INCREMENT,
  `nom_classe` varchar(50) NOT NULL,
  `structure_id` int(11) NOT NULL,
  PRIMARY KEY (`id_classe`),
  KEY `classes_Structures_FK` (`structure_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `classes`
--

INSERT INTO `classes` (`id_classe`, `nom_classe`, `structure_id`) VALUES
(1, 'classe1', 1),
(2, 'classe2', 2),
(3, 'classe3', 2),
(4, 'classe4', 1),
(5, 'classe5', 2),
(6, 'classe6', 1),
(7, 'classe6', 2),
(8, 'classe7', 1),
(9, 'Valarep-classe1', 1),
(10, 'Valarep-classe2', 2),
(11, 'Dampierre-classe1', 2),
(12, 'Dampierre-classe2', 2);

-- --------------------------------------------------------

--
-- Structure de la table `formateurs`
--

DROP TABLE IF EXISTS `formateurs`;
CREATE TABLE IF NOT EXISTS `formateurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_formateur` varchar(50) NOT NULL,
  `prenom_formateur` varchar(50) NOT NULL,
  `mail_formateur` varchar(100) NOT NULL,
  `login` varchar(25) NOT NULL,
  `Mot_passe` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `formateurs`
--

INSERT INTO `formateurs` (`id`, `nom_formateur`, `prenom_formateur`, `mail_formateur`, `login`, `Mot_passe`) VALUES
(1, 'Defer', 'Tanguy', '', 'test', 'test'),
(2, 'nanar', 'beber', '', 'nanar', 'beber'),
(3, 'jean', 'jean', '', '1', '1');

-- --------------------------------------------------------

--
-- Structure de la table `formes`
--

DROP TABLE IF EXISTS `formes`;
CREATE TABLE IF NOT EXISTS `formes` (
  `id_forme` int(11) NOT NULL AUTO_INCREMENT,
  `nom_forme` varchar(50) NOT NULL,
  `prenom_forme` varchar(50) NOT NULL,
  `photo_forme` varchar(100) DEFAULT NULL,
  `id_classe` int(11) NOT NULL,
  `id_societe` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_forme`),
  KEY `Formes_classes_FK` (`id_classe`),
  KEY `Formes_societes_FK` (`id_societe`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `formes`
--

INSERT INTO `formes` (`id_forme`, `nom_forme`, `prenom_forme`, `photo_forme`, `id_classe`, `id_societe`) VALUES
(1, 'test_nom', 'test_prenom', 'forme1.jpeg', 3, 1),
(2, 'jean', 'jean', 'forme1.jpeg', 1, 3),
(4, 'pierre', 'pier', 'forme1.jpeg', 1, 3),
(5, 'paul', 'popo', 'forme1.jpeg', 1, 1),
(6, 'jeanne', 'j', 'forme1.jpeg', 2, 3),
(7, 'bea', 'b', 'forme1.jpeg', 2, 1),
(8, 'bertrand', 'ber', 'forme1.jpeg', 1, 1),
(9, 'philipe', 'phil', 'forme1.jpeg', 1, 3),
(10, 'philo', 'philt', 'forme1.jpeg', 1, 3),
(11, 'stan', 'stany', 'forme1.jpeg', 1, 1),
(26, 'bertrand', 'ber', 'forme1.jpeg', 1, 3),
(27, 'philipe', 'phil', 'forme1.jpeg', 1, 3),
(30, 'test_nom', 'test_prenom', 'forme1.jpeg', 3, 1),
(36, 'test_nom', 'test_prenom', 'forme1.jpeg', 3, 1),
(38, 'stan', 'stany', 'forme1.jpeg', 1, 1),
(39, 'bertrand', 'ber', 'forme1.jpeg', 1, 3),
(40, 'philipe', 'phil', 'forme1.jpeg', 1, 3),
(41, 'jean', 'jean', 'forme1.jpeg', 1, 3),
(42, 'pierre', 'pier', 'forme1.jpeg', 1, 3),
(43, 'paul', 'popo', 'forme1.jpeg', 1, 1),
(44, 'jeanne', 'j', 'forme1.jpeg', 2, 3),
(45, 'pierre', 'pier', 'forme1.jpeg', 1, 3),
(46, 'paul', 'popo', 'forme1.jpeg', 1, 1),
(47, 'sans ', 'societe', 'forme1.jpeg', 2, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `id_groupe` int(11) NOT NULL AUTO_INCREMENT,
  `id_formateur` int(11) NOT NULL,
  `id_classe` int(11) NOT NULL,
  PRIMARY KEY (`id_groupe`),
  KEY `id_formateur` (`id_formateur`),
  KEY `id_classe` (`id_classe`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`id_groupe`, `id_formateur`, `id_classe`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 3, 3),
(4, 2, 1),
(5, 1, 5),
(6, 3, 6),
(7, 1, 3),
(8, 1, 4),
(9, 1, 6),
(10, 1, 7),
(11, 1, 8);

-- --------------------------------------------------------

--
-- Structure de la table `societes`
--

DROP TABLE IF EXISTS `societes`;
CREATE TABLE IF NOT EXISTS `societes` (
  `id_societe` int(11) NOT NULL AUTO_INCREMENT,
  `nom_societe` varchar(100) DEFAULT NULL,
  `num` varchar(50) DEFAULT NULL,
  `rue` varchar(50) DEFAULT NULL,
  `code_postal` int(5) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_societe`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `societes`
--

INSERT INTO `societes` (`id_societe`, `nom_societe`, `num`, `rue`, `code_postal`, `ville`) VALUES
(1, 'test', '20', 'rue de la rue', 59300, 'Valenciennes'),
(2, 'Aucune société', '0', '0', 0, '0'),
(3, 'pas de société', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `structures`
--

DROP TABLE IF EXISTS `structures`;
CREATE TABLE IF NOT EXISTS `structures` (
  `id_structure` int(11) NOT NULL AUTO_INCREMENT,
  `nom_structure` varchar(100) NOT NULL,
  PRIMARY KEY (`id_structure`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `structures`
--

INSERT INTO `structures` (`id_structure`, `nom_structure`) VALUES
(1, 'VALAREP'),
(2, 'DAMPIERE');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `classes_Structures_FK` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id_structure`);

--
-- Contraintes pour la table `formes`
--
ALTER TABLE `formes`
  ADD CONSTRAINT `Formes_classes_FK` FOREIGN KEY (`id_classe`) REFERENCES `classes` (`id_classe`),
  ADD CONSTRAINT `Formes_societes_FK` FOREIGN KEY (`id_societe`) REFERENCES `societes` (`id_societe`) ON UPDATE SET NULL;

--
-- Contraintes pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD CONSTRAINT `groupe_ibfk_1` FOREIGN KEY (`id_formateur`) REFERENCES `formateurs` (`id`),
  ADD CONSTRAINT `groupe_ibfk_2` FOREIGN KEY (`id_classe`) REFERENCES `classes` (`id_classe`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
