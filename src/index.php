<?php

namespace AppliValarep;

require "vendor/autoload.php";

use AppliValarep\Model\Classes\Classe;
use AppliValarep\Model\Classes\Eleve;
use AppliValarep\Model\Classes\Membre;
use AppliValarep\Model\Classes\Session;
use AppliValarep\Model\Classes\Groupe;
use AppliValarep\Controller\ClasseController;
use AppliValarep\Controller\EleveController;
use AppliValarep\Controller\MembreController;

$page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);
$debugMode = true;

if (!isset($page))
{
    $page = "login";
}

switch ($page)
{
    case "login":
        include "src/View/templates/login.html";
        break;
    case "membre-login":
        $username = filter_input(INPUT_POST, 'username');
        $password = filter_input(INPUT_POST, 'password');
        MembreController::login($username, $password);
        break;
    case "classes":
        $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
        ClasseController::listAction($id);
        break;
    case "classe-add":
        ClasseController::addAction();
        break;
    case "classe-add-submit":
        ClasseController::addSubmitAction();
        break;
    case "classe-remove":
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        ClasseController::removeAction($id);
        break;
    case "classe-remove-submit":
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        ClasseController::removeSubmitAction($id);
        break;
    case "classes_valarep":
        ClasseController::listValarep();
        break;
    case "classes_dampierre":
        ClasseController::listDampierre();
        break;     
    case "eleves_valarep":
        EleveController::elevesValarep();
        break;
    case "eleves_dampierre":
        EleveController::elevesDampierre();
        break;
    case "classe":
        $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
        EleveController::listAction($id);
        break;
    case "élève":
        $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
        EleveController::readAction($id);
        break;
    case "deconnexion":
        MembreController::deconnexion();
        break;
    default:
        $message = "Erreur 404 : Page inconnue&nbsp;!";
        include "View/templates/erreur.html";
        break;
}
