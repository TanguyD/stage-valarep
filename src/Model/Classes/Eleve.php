<?php
namespace AppliValarep\Model\Classes;

use AppliValarep\Model\Dao\EleveDao;
use \InvalidArgumentException;


class Eleve extends Classe
{
    protected $id_forme;
    protected $nom_forme;
    protected $prenom_forme;
    protected $photo_forme;
    protected $id_classe;
    protected $id_societe;
    protected $nom_societe;
    protected $num;
    protected $rue;
    protected $code_postal;
    protected $ville;
    protected $nom_classe;



    protected function get_id_forme() {return $this->id_forme; }
    protected function set_id_forme($value) {$this->id_forme = $value; }

    protected function get_nom_forme() {return $this->nom_forme; }
    protected function set_nom_forme($value) {$this->nom_forme = $value; }

    protected function get_prenom_forme() {return $this->prenom_forme; }
    protected function set_prenom_forme($value) {$this->prennom_forme = $value; }

    protected function get_photo_forme() {return $this->photo_forme; }
    protected function set_photo_forme($value) {$this->photo_forme = $value; }

    protected function get_id_classe() {return $this->id_classe; }
    protected function set_id_classe($value) {$this->nom_forme = $value; }

    
    protected function get_id_societe() {return $this->id_societe; }
    protected function set_id_societe($value) {$this->id_societe = $value; }

    protected function get_nom_societe() {return $this->nom_societe; }
    protected function set_nom_societe($value) {$this->nom_societe = $value; }

    protected function get_num() {return $this->num; }
    protected function set_num($value) {$this->num = $value; }

    protected function get_rue() {return $this->rue; }
    protected function set_rue($value) {$this->rue = $value; }

    protected function get_code_postal() {return $this->code_postal; }
    protected function set_code_postal($value) {$this->code_postal = $value; }

    protected function get_ville() {return $this->ville; }
    protected function set_ville($value) {$this->ville = $value; }

    protected function get_nom_classe() {return $this->nom_classe; }
    protected function set__nom_classe($value) {$this->nom_classe = $value; }


    public function __construct()
    {
        $nb = func_num_args();
        switch ($nb) {
            case 0:
                $this->construct_0();
                break;
            case 6:
                $id_forme = func_get_arg(0);
                $nom_forme = func_get_arg(1);
                $prenom_forme = func_get_arg(2);
                $photo_forme = func_get_arg(3);
                $id_classe = func_get_arg(4);
                $id_societe = func_get_arg(5);
                $this->construct_6($id_forme, $nom_forme, $prenom_forme, $photo_forme, $id_classe, $id_societe);
                break;
            default:
                throw new InvalidArgumentException("Invalid parameters number");
                break;
        }
    }

    private function construct_0()
    {
        // valeurs par défaut
        $this->id_forme = null;
        $this->nom_forme = "";
        $this->prenom_forme = "";
        $this->photo_forme = "";
        $this->id_classe = null;
        $this->id_societe = null;
    }

    private function construct_6($id_forme, $nom_forme, $prenom_forme, $photo_forme, $id_classe, $id_societe)
    {
        // valeurs fournies en paramètres
        $this->id_forme = $id_forme;
        $this->nom_forme = $nom_forme;
        $this->prenom_forme = $prenom_forme;
        $this->photo_forme = $photo_forme;
        $this->id_classe = $id_classe;
        $this->id_societe = $id_societe;

        // valeurs fournies en paramètres;
    }

    public function __toString()
    {
        return $this->nom_forme;
    }

    public static function getAllEleves($id)
    {

        $dao = new EleveDao();
        return $dao->getAll($id);
    }

    public static function getValarep()
    {

        $dao = new EleveDao();
        return $dao->getValarep();
    }

    public static function getDampierre()
    {

        $dao = new EleveDao();
        return $dao->getDampierre();
    }

    public static function get($id)
    {

        $dao = new EleveDao();
        return $dao->get($id);
    }
}

