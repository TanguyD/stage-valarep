<?php


namespace AppliValarep\Model\Classes;

use AppliValarep\Model\Dao\AuthDao;
use \InvalidArgumentException;


class Session extends Objet
{
    
        protected $id;
        protected $nom_formateur;
        protected $prenom_formateur;
        protected $mail_formateur;
        protected $login;
        protected $Mot_passe;
    
    
        protected function get_id(){return $this->id;}
        protected function set_id($value){$this->id = $value;}
    
        protected function get_nom_formateur(){return $this->nom_formateur;}
        protected function set_nom_formateur($value){$this->nom_formateur = $value;}

        protected function get_prenom_formateur(){return $this->prenom_formateur;}
        protected function set_prenom_formateur($value){$this->prenom_formateur = $value;}
    
        protected function get_mail_formateur(){return $this->mail_formateur;}
        protected function set_mail_formateur($value){$this->mail_formateur = $value;}
    
        protected function get_login(){return $this->login;}
        protected function set_login($value){$this->login = $value;}
    
        protected function get_Mot_passe(){return $this->Mot_passe;}
        protected function set_Mot_passe($value){$this->Mot_passe = $value;}

    public function __construct()
    {
        $nb = func_num_args();
        switch ($nb) {
            case 0:
                $this->construct_0();
                break;
            case 6:
                $id = func_get_arg(0);
                $nom_formateur = func_get_arg(1);
                $prenom_formateur = func_get_arg(2);
                $mail_formateur = func_get_arg(3);
                $login = func_get_arg(4);
                $Mot_passe = func_get_arg(5);
                $this->construct_6($id, $nom_formateur, $prenom_formateur, $mail_formateur, $login, $Mot_passe);
                break;
            default:
                throw new InvalidArgumentException("Invalid parameters number");
                break;
        }
    }

    private function construct_0()
    {
        // valeurs par défaut
        $this->id = null;
        $this->nom_formateur = "";
        $this->prenom_formateur = "";
        $this->mail_formateur = "";
        $this->login = "";
        $this->Mot_passe = "";
    }

    private function construct_6($id, $nom_formateur, $prenom_formateur, $mail_formateur, $login, $Mot_passe)
    {
        // valeurs fournies en paramètres
        $this->id = $id;
        $this->nom_formateur = $nom_formateur;
        $this->prenom_formateur = $prenom_formateur;
        $this->mail_formateur = $mail_formateur;
        $this->login = $login;
        $this->Mot_passe = $Mot_passe;
    }

    public function __toString()
    {
        return $this->nom_formateur;
    }

    public static function getAll($username, $password)
    {
        $dao = new AuthDao();
        return $dao->getAll($username, $password);
    }

    public static function login($username, $password)
    {

        $dao = new AuthDao();
        return $dao->login($username, $password);
    }


}