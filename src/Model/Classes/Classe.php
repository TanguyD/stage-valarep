<?php
namespace AppliValarep\Model\Classes;

use AppliValarep\Model\Dao\ClasseDao;
use \InvalidArgumentException;
session_start();

class Classe extends Session
{
    protected $id_classe;
    protected $nom_classe;
    protected $structure_id;
    protected $id_structure;
    protected $nom_structure;



    protected function get_id_classe() {return $this->id_classe; }
    protected function set_id_classe($value) {$this->id_classe = $value; }

    protected function get_nom_classe() {return $this->nom_classe; }
    protected function set_nom_classe($value) {$this->nom_classe = $value; }

    protected function get_id_structure() {return $this->id_structure; }
    protected function set_id_structure($value) {$this->id_structure = $value; }

    protected function get_nom_structure() {return $this->nom_structure; }
    protected function set_nom_structure($value) {$this->nom_structure = $value; }



    public function __construct()
    {
        $nb = func_num_args();
        switch ($nb) {
            case 0:
                $this->construct_0();
                break;
            case 3:
                $id_classe = func_get_arg(0);
                $nom_classe = func_get_arg(1);
                $structure_id = func_get_arg(2);
                $this->construct_3($id_classe, $nom_classe, $structure_id);
                break;
            default:
                throw new InvalidArgumentException("Invalid parameters number");
                break;
        }
    }

    private function construct_0()
    {
        // valeurs par défaut
        $this->id_classe = null;
        $this->nom_classe = "";
        $this->structure_id = null;
    }

    private function construct_3($id_classe, $nom_classe, $structure_id)
    {
        // valeurs fournies en paramètres
        $this->id_classe = $id_classe;
        $this->nom_classe = $nom_classe;
        $this->structure_id = $structure_id;
    }

    public function __toString()
    {
        return $this->nom_classe;
    }

    public static function getAlls($id)
    {

        $dao = new ClasseDao();
        return $dao->getAll($id);
    }
    public static function getId($id)
    {

        $dao = new ClasseDao();
        return $dao->getId($id);
    }
    public static function getValarep()
    {

        $dao = new ClasseDao();
        return $dao->getValarep();
    }
    public static function getDampierre()
    {

        $dao = new ClasseDao();
        return $dao->getDampierre();
    }

    public static function get($id_classe)
    {

        $dao = new ClasseDao();
        return $dao->get($id_classe);
    }
    //Ajouter une classe
    public static function insert($nom_classe, $structure_id)
    {
        $dao = new ClasseDao();
        return $dao->insert($nom_classe, $structure_id);
    }
    //Supprimer une classe
    public static function delete($id)
    {
        $dao = new ClasseDao();
        return $dao->delete($id);
    }
}

