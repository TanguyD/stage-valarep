<?php

namespace AppliValarep\Model\Dao;

use AppliValarep\Model\Dal\Dal;
use \PDO;

class ClasseDao extends Dal
{
    private $classname = "AppliValarep\\Model\\Classes\\Classe";
    private $table = "classes";
    private $structure = ['id_classe', 'nom_classe', 'structure_id'];

    public function getAlls($id)
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  ;";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $rows = $stmt->fetchAll();
        $this->Close();
        return $rows;
    }

    public function getValarep()
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  LEFT JOIN `structures`
                  ON `classes`.`structure_id`=`structures`.`id_structure`
                  WHERE `structure_id` = 1
                  ORDER BY `nom_classe`;";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $rows = $stmt->fetchAll();
        $this->Close();
        return $rows;
    }

    public function getDampierre()
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  LEFT JOIN `structures`
                  ON `classes`.`structure_id`=`structures`.`id_structure`
                  WHERE `structure_id` = 2
                  ORDER BY `nom_classe`;";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $rows = $stmt->fetchAll();
        $this->Close();
        return $rows;
    }
    public function get($id)
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  LEFT JOIN `structures` ON `classes`.`structure_id` = `structures`.`id_structure`
                  WHERE `id_classe` = :id;";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $object = $stmt->fetch();
        $this->Close();
        return $object;
    }

    public function insert($nom_classe, $structure_id)
    {
        global $debugMode;
        $nbRows = 0;



        {
            $query = "INSERT INTO `{$this->table}`
            (`nom_classe`, `structure_id`)
            VALUES
            (:nom_classe, :structure_id);
            ";

            $this->Open();
            $stmt = $this->dbh->prepare($query);

            $stmt->bindParam(":nom_classe", $nom_classe, PDO::PARAM_STR);
            $stmt->bindParam(":structure_id", $structure_id, PDO::PARAM_INT);

            $nbRows = $stmt->execute();


            if ($debugMode && $nbRows != 1) {
                    echo '<div class="alert alert-danger" role="alert">' . "\n";
                    var_dump($structure_id);
                    echo $stmt->errorInfo()[2];
                    echo '</div>' . "\n";
                }

            $this->Close();
        }
        return $nbRows;
    }

    public function delete($id)
    {
        global $debugMode;

        $query = "DELETE FROM `{$this->table}`
                  WHERE `id_classe` = :id; 
                ";

        $this->Open();
        $stmt = $this->dbh->prepare($query);

        $stmt->bindParam(":id", $id, PDO::PARAM_INT);

        $nbRows = $stmt->execute();

        if ($debugMode && $nbRows != 1) {
                echo '<div class="alert alert-danger" role="alert">' . "\n";
                echo $stmt->errorInfo()[2];
                echo '</div>' . "\n";
            }

        $this->Close();
        return $nbRows;
    }
}

