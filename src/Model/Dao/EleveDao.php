<?php

namespace AppliValarep\Model\Dao;

use AppliValarep\Model\Dal\Dal;
use \PDO;

class EleveDao extends Dal
{
    private $classname = "AppliValarep\\Model\\Classes\\Eleve";
    private $table = "formes";
    private $structure = ['id_forme', 'nom_forme', 'prenom_forme', 'photo_forme', 'id_classe', 'id_societe'];

    public function getAll($id)
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  WHERE `id_classe` = :id
                  order by `nom_forme`;
                  ";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $rows = $stmt->fetchAll();
        $this->Close();
        return $rows;
    }

    public function getValarep()
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  LEFT JOIN `classes`
                  ON `formes`.`id_classe`=`classes`.`id_classe`
                  LEFT JOIN `structures`
                  ON `classes`.`structure_id`=`structures`.`id_structure`
                  WHERE `structure_id` = 1
                  order by `nom_forme`;
                  ";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $rows = $stmt->fetchAll();
        $this->Close();
        return $rows;
    }

    public function getDampierre()
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  LEFT JOIN `classes`
                  ON `formes`.`id_classe`=`classes`.`id_classe`
                  WHERE `structure_id` = 2
                  order by `nom_forme`;
                  ";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $rows = $stmt->fetchAll();
        $this->Close();
        return $rows;
    }

    public function get($id)
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  LEFT JOIN `SOCIETES`
                  ON `formes`.`id_societe` = `societes`.`id_societe` 
                  WHERE `id_forme` = :id;
                  ";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $object = $stmt->fetch();
        $this->Close();
        return $object;
    }
}

