<?php

namespace AppliValarep\Model\Dao;

use AppliValarep\Model\Dal\Dal;
use \PDO;

class StructureDao extends Dal
{
    private $classname = "AppliValarep\\Model\\Classes\\Structure";
    private $table = "structures";
    private $structure = ['id_structure', 'nom_structure'];

    public function insertStructure($id_structure, $nom_structure)
    {
        global $debugMode;
        $nbRows = 0;


        {
            $query = "INSERT INTO `{$this->table}`
            (`id_structure`, `nom_structure`)
            VALUES
            (:id_structure, :nom_structure);
            ";

            $this->Open();
            $stmt = $this->dbh->prepare($query);

            $stmt->bindParam(":id_structure", $id_structure, PDO::PARAM_INT);
            $stmt->bindParam(":nom_structure", $nom_structure, PDO::PARAM_STR);

            $nbRows = $stmt->execute();

            if ($debugMode && $nbRows != 1) {
                    echo '<div class="alert alert-danger" role="alert">' . "\n";
                    echo $stmt->errorInfo()[2];
                    echo '</div>' . "\n";
                }

            $this->Close();
        }
        return $nbRows;
    }
}

