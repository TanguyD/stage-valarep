<?php

namespace AppliValarep\Model\Dao;

use AppliValarep\Model\Dal\Dal;
use \PDO;

class AuthDao extends Dal
{
    private $classname = "AppliValarep\\Model\\Classes\\Session";
    private $table = "formateurs";
    private $structure = ['id', 'nom_formateur', 'prenom_formateur', 'mail_formateur', 'login', 'Mot_passe'];

    public function getAll($username, $password)
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  WHERE `login` = :username AND `Mot_passe` = MD5 (:password);";

        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam(":username", $username, PDO::PARAM_STR);
        $stmt->bindParam(":password", $password, PDO::PARAM_STR);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $rows = $stmt->fetchAll();
        $this->Close();
        return $rows;
    }

    public function login($username, $password)
    {

        $query = "SELECT * FROM `{$this->table}` WHERE `login` = :username AND `Mot_passe` = MD5 (:password) ;";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam(":username", $username, PDO::PARAM_STR);
        $stmt->bindParam(":password", $password, PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetchColumn();
        $this->Close();

        if ($rows != 0) // nom d'utilisateur et mot de passe correctes
            {
                $_SESSION['username'] = $username;
                return $_SESSION['username'];
            } else {
                echo 'Vous n\'êtes pas autorisé à acceder à cette zone';
                include('src/View/templates/login.html'); // utilisateur ou mot de passe incorrect
            }
    }
}

