<?php

namespace AppliValarep\Model\Dao;

use AppliValarep\Model\Dal\Dal;
use \PDO;

class GroupeDao extends Dal
{
    private $classname = "AppliValarep\\Model\\Classes\\Groupe";
    private $table = "groupe";
    private $structure = ['id_groupe', 'id_formateur', 'id_classe', ];

    public function getAll($id)
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  LEFT JOIN `formateurs`
                  ON `groupe`.`id_formateur`=`formateurs`.`id`
                  INNER JOIN `classes`
                  ON `groupe`.`id_classe` = `classes`.`id_classe`
                  INNER JOIN `structures`
                  ON `classes`.`structure_id` = `structures`.`id_structure`
                  WHERE `id_formateur` = :id
                  ORDER BY `nom_classe`;";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $rows = $stmt->fetchAll();
        $this->Close();
        return $rows;
    }
}

