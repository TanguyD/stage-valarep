<?php
if (!isset($_SESSION['username']) && !isset($_SESSION['password'])) {
    echo 'Vous n\'êtes pas autorisé à acceder à cette zone';
    include('src/View/templates/login.html');
    exit;
}
?>

<!DOCTYPE html>
<html lang="fr" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>
        <?php if ($action == "classe-membre") : ?>
        Vos
        <?= $type_pluriel;  ?>
        <?php elseif ($action == "valarep" || $action == "dampierre") : ?>
        les
        <?= $type_pluriel; ?> de <?= $structure; ?>
        <?php endif; ?>
    </title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,500" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="src/View/images/icons/apple-touch-icon.png" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="src/View/css/style_list_classe.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!--===============================================================================================-->


</head>
<div class="bloc">
    <div class="titre">
        <h1 class="lead">
            <?php if ($action == "classe-membre") : ?>
            <h1>Vos
                <?= $type_pluriel;  ?>
            </h1>
            <?php elseif ($action == "valarep") : ?>
            <h1>les
                <?= $type_pluriel; ?> de
                <?= $structure; ?>
            </h1>
            <?php elseif ($action == "dampierre") : ?>
            <h1>les
                <?= $type_pluriel; ?> de
                <?= $structure; ?>
            </h1>
            <?php endif; ?>
        </h1>
    </div>


    <!-- NAV-->
    <span class="nav">
        <?php include "navbar.html.php"; ?></span>

    <body>

        <?php if ($_SESSION['username'] == "test") :  ?>
        <?php if ($action == "valarep" || $action == "dampierre") : ?>

        <form action="?page=<?= $type; ?>-add" method="POST" class="btn-add">
            <button type="submit" name="btn_add" class="btn btn-success">
                <span class="fas fa-plus"></span> Ajouter une classe
            </button>
        </form>

        <?php endif; ?>
        <?php endif; ?>

        <section class="content">
            <div style="overflow-y: scroll; height:600px; ">
                <div class="overlay overlay-bg"></div>
                <div class="container">
                    <div class="row fullscreen align-items-center justify-content-between">
                        <div class="col-lg-6 col-md-7 col-sm-8">
                            <div class="banner-content">
                                <table class="tableau" data-vertable="ver1">
                                    <thead>
                                        <tr class="row200 head">
                                            <th class="column100 column2" data-column="column2"></th>
                                            <th class="column100 column3" data-column="column3"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rows as $classe) :  ?>

                                        <tr class="row100" data-href="?page=<?= $type; ?>&id=<?= $classe->id_classe; ?>">
                                            <td class="column100 column1" data-column="column1">

                                                <?= $classe->nom_classe; ?></a></strong></span>
                                            </td>
                                            <td class="column100 column4" data-column="column2">

                                                <?= $classe->nom_structure; ?></a></strong></span>
                                            </td>

                                            <?php if ($_SESSION['username'] == "test") :  ?>
                                            <?php if ($action == "valarep" || $action == "dampierre") : ?>
                                            <th>
                                                <form action="?page=<?= $type; ?>-edit" method="POST">
                                                    <input type="hidden" name="id" value="<?= $classe->id_classe; ?>">
                                                    <button type="submit" name="btn_edit" class="btn btn-primary">
                                                        <span class="fas fa-edit"></span> Editer
                                                    </button>
                                                </form>
                                            </th>
                                            <th>
                                                <form action="?page=<?= $type; ?>-remove" method="POST">
                                                    <input type="hidden" name="id" value="<?= $classe->id_classe; ?>">
                                                    <button type="submit" name="btn_remove" class="btn btn-danger">
                                                        <span class="fas fa-trash-alt"></span> Supprimer
                                                    </button>
                                                </form>
                                            </th>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
</section>

</body>
<footer>
    <!-- Bouton RETOUR-->
    <span class="text">
        <?php include "html_foot_retour.html"; ?></span>


    <!-- Bouton AIDE-->

    <body class="back">
        <div class="btn from-left"><span data-toggle="modal" data-target="#modalRelatedContent" class="text">AIDE</span></div>
    </body>
    <!--Modal: modalRelatedContent-->
    <div class="modal fade right" id="modalRelatedContent" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
        <div class="modal-dialog modal-side modal-bottom-right modal-notify modal-info" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->

                <!--Body-->
                <div class="modal-body">

                    <div class="row">

                        <div class="col-7">
                            <p><strong>Séléctionnez une classe pour afficher la liste des élèves de celle-ci.</strong></p>
                            <p><strong>le menu à droite vous permet d'afficher toutes les listes disponibles.</strong></p>
                            <button style="color: darkorange ;" type="button" class="close" data-dismiss="modal" aria-label="Close">FERMER
                        </div>
                    </div>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
</footer>
<!--Modal: modalRelatedContent-->

<!--********SCRIPT******-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="src/View/templates/landing_page/js/vendor/bootstrap.min.js"></script>
<script src="src/View/templates/landing_page/js/jquery.ajaxchimp.min.js"></script>
<script src="src/View/templates/landing_page/js/owl.carousel.min.js"></script>
<script src="src/View/templates/landing_page/js/jquery.nice-select.min.js"></script>
<script src="src/View/templates/landing_page/js/jquery.magnific-popup.min.js"></script>
<script src="src/View/templates/landing_page/js/main.js"></script>



<script>
    $(document).ready(function($) {
        $(".row100").click(function() {
            window.document.location = $(this).data("href");
        });
    });

    $(function() {
        $('a[href*=#]').on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top
            }, 500, 'linear');
        });
    });
</script>


</body>

</html>

<section id="section07" class="demo">

    <p><span></span><span></span><span></span></p>
</section> 