<?php

if (!isset($_SESSION['username']) && !isset($_SESSION['password'])) {
    echo 'Vous n\'êtes pas autorisé à acceder à cette zone';
    include('src/View/templates/login.html');
    exit;
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Liste des élèves</title>
    <meta charset="UTF-8">

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,500" rel="stylesheet">
    <!--CSS============================================= -->
    <link rel="icon" type="image/png" href="src/View/images/icons/apple-touch-icon.png" />
    <link rel="stylesheet" href="src/View/templates/landing_page/css/linearicons.css">
    <link rel="stylesheet" href="src/View/templates/landing_page/css/owl.carousel.css">
    <link rel="stylesheet" href="src/View/templates/landing_page/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="src/View/css/style_aff_eleve.css">
    <!--===============================================================================================-->
</head>
<header><?php include "navbar.html.php"; ?></header>
<div class="starter-template">
    <div class="titre">
        <h1 class="lead">
            <?php if ($action == "eleve-classe") : ?>
            les <?= $type_pluriel; ?> de la classe : "<?= $classe->nom_classe; ?>"-<?= $classe->nom_structure;  ?>
            <?php elseif ($action == "valarep") : ?>
            <h1>les <?= $type_pluriel; ?> de <?= $structure; ?></h1>
            <?php elseif ($action == "dampierre") : ?>
            <h1>les <?= $type_pluriel; ?> de <?= $structure; ?></h1>
            <?php endif; ?>
        </h1>
    </div>

    <body>

        <!------ Include the above in your HEAD tag ---------->
        <div class="container">
            <div style="overflow-y: scroll; overflow-x: hidden; height:600px; width:1400px;">
                <div class="row">
                    <section class="content">
                        <div class="table-container">
                            <table class="table table-filter">
                                <tbody>
                                    <?php foreach ($rows as $eleve) : ?>
                                    <tr class="row100" data-href="?page=<?= $type; ?>&id=<?= $eleve->id_forme; ?>">
                                        <td>
                                            <div class="media">
                                                <a class="pull-left">
                                                    <img src="src/View/images/<?= $eleve->photo_forme; ?>" class="media-photo">
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="title">
                                                        <?= $eleve->nom_forme; ?> <?= $eleve->prenom_forme; ?>
                                                    </h4>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!-- Boutons retour/deco-->
        <span class="test"><?php include "html_foot_retour.html"; ?></span>

        <!--Bouton AIDE-->
        <div class="btn_aide">

            <body class="back">
                <div class="btn from-left"><span data-toggle="modal" data-target="#modalRelatedContent" class="text">AIDE</span></div>
            </body>
            <div class="modal fade right" id="modalRelatedContent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">
                <div class="modal-dialog modal-side modal-bottom-right modal-notify modal-info" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="col-7">
                                <p><strong>Passez votre curseur sur la photo d'un élève pour l'agrandir.</strong></p>
                                <p><strong>Séléctionnez un élève pour avoir sa fiche détaillée.</strong></p>
                                <p><strong>le menu à droite vous permet d'afficher toutes les listes disponibles.</strong></p>
                                <button style="color: darkorange ;" type="button" class="close" data-dismiss="modal" aria-label="Close">FERMER
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal: modalRelatedContent-->


        <!--Chevrons scroll-->
        <section id="section07" class="demo">

            <p><span></span><span></span><span></span></p>
        </section>

        <script>
            $(document).ready(function($) {
                $(".row100").click(function() {
                    window.document.location = $(this).data("href");
                });
            });
            $(function() {
                $('a[href*=#]').on('click', function(e) {
                    e.preventDefault();
                    $('html, body').animate({
                        scrollTop: $($(this).attr('href')).offset().top
                    }, 500, 'linear');
                });
            });
        </script>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="src/View/templates/landing_page/js/vendor/jquery-2.2.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="src/View/templates/landing_page/js/vendor/bootstrap.min.js"></script>
        <script src="src/View/templates/landing_page/js/jquery.ajaxchimp.min.js"></script>
        <script src="src/View/templates/landing_page/js/owl.carousel.min.js"></script>
        <script src="src/View/templates/landing_page/js/jquery.nice-select.min.js"></script>
        <script src="src/View/templates/landing_page/js/jquery.magnific-popup.min.js"></script>
        <script src="src/View/templates/landing_page/js/main.js"></script>
    </body>

</html> 