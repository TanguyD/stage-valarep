<?php

if (!isset($_SESSION['username'])) {
	echo 'Vous n\'êtes pas autorisé à acceder à cette zone';
	include('src/View/templates/login.html');
	exit;
}

?>
<main role="main" class="container">
    <h1>Supprimer un <?= $type; ?></h1>
    <form action="?page=<?= $type ?>-remove-submit" method="post">
        <input type="hidden" name="id" value="<?= $classe->id_classe; ?>">
        <div class="form-group text-center">
            <p style="font-size:1.5rem;">
                Êtes-vous sûr de bien vouloir supprimer la <?= $type; ?> <?= $classe->nom_classe; ?>&nbsp;?
            </p>
            <button class="btn btn-danger" type="submit" name="btn_yes">Oui</button>
            <button class="btn btn-light" type="submit" name="btn_no">Non</button>
        </div>
    </form>
</main><!-- /.container -->
<?php include "html_foot_retour.html"; ?> 