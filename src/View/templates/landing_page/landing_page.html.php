<?php

if (!isset($_SESSION['username'])) {
	echo 'Vous n\'êtes pas autorisé à acceder à cette zone';
	include('src/View/templates/login.html');
	exit;
}

?>

<!DOCTYPE html>
<html lang="fr" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="src/View/templates/landing_page/img/fav.png">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Accueil</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,500" rel="stylesheet">
    <!--
		CSS
		============================================= -->
    <link rel="icon" type="image/png" href="src/View/images/icons/apple-touch-icon.png" />
    <link rel="stylesheet" href="src/View/templates/landing_page/css/linearicons.css">
    <link rel="stylesheet" href="src/View/templates/landing_page/css/owl.carousel.css">
    <link rel="stylesheet" href="src/View/templates/landing_page/css/font-awesome.min.css">
    <link rel="stylesheet" href="src/View/templates/landing_page/css/nice-select.css">
    <link rel="stylesheet" href="src/View/templates/landing_page/css/magnific-popup.css">
    <link rel="stylesheet" href="src/View/templates/landing_page/css/bootstrap.css">
    <link rel="stylesheet" href="src/View/templates/landing_page/css/main.css">
</head>

<body class="dup-body">
    <div class="dup-body-wrap">
        <!-- Start Header Area -->
        <header class="default-header">
            <div class="header-wrap">
                <div class="header-top d-flex justify-content-between align-items-center">
                    <div class="logo">
                    </div>
                    <div class="main-menubar d-flex align-items-center">

                        <nav class="hide">
                            <a href="?page=classes_valarep">Toutes les classes | Valarep</a>
                            <a href="?page=classes_dampierre">Toutes les classes | Dampierre</a>
                            <a href="?page=eleves_valarep">Tous les élèves | Valarep</a>
                            <a href="?page=eleves_dampierre">Tous les élèves | Dampierre</a>
                        </nav>
                        <div class="menu-bar">
                            <h4>MENU</h4><span class="lnr lnr-menu"></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- End Header Area -->
        <!-- Start Banner Area -->
        <section class="banner-area relative">
            <div class="overlay overlay-bg"></div>
            <div class="container">
                <div class="row fullscreen align-items-center justify-content-between">
                    <div class="col-lg-6 col-md-7 col-sm-8">
                        <div class="banner-content">
                            <?php foreach ($rows as $membre) : ?>
                            <h1><?php echo "Bonjour $membre->prenom_formateur, bienvenue sur votre portail"; ?> </h1>
                            <p>
                                - En cliquant sur le bouton ci-dessous vous accèderez à liste de vos classes,
                                séléctionnez en une pour afficher la liste des élèves de cette dernière,
                                enfin, séléctionnez un élève pour afficher sa fiche détaillée.<br>
                                - Dans le coin suppérieur droit se trouve le menu, il vous permet d'accéder à l'ensemble
                                des listes disponibles.<br>
                                - Le bouton AIDE vous fournit des astuces sur les différentes pages.<br>
                                <em><strong>Pour toutes demande d'informations contactez</strong></em> <a style="color : red; text-decoration : underline;" href="mailto:tanguy59192@hotmail.fr" title="Envoyer un mail">l'Admin.</a></p>

                            <a href="?page=classes&id=<?= $membre->id; ?>" class="primary-btn">C'est parti<span class="lnr lnr-arrow-right"></span></a>
                            <form class="login100-form validate-form" method="POST" class="form-inline"></form>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-4">
                        <img src="src/View/templates/landing_page/img/banner1.png" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->
        <!-- Start studio Area -->

        <script src="src/View/templates/landing_page/js/vendor/jquery-2.2.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="src/View/templates/landing_page/js/vendor/bootstrap.min.js"></script>
        <script src="src/View/templates/landing_page/js/jquery.ajaxchimp.min.js"></script>
        <script src="src/View/templates/landing_page/js/owl.carousel.min.js"></script>
        <script src="src/View/templates/landing_page/js/jquery.nice-select.min.js"></script>
        <script src="src/View/templates/landing_page/js/jquery.magnific-popup.min.js"></script>
        <script src="src/View/templates/landing_page/js/main.js"></script>
</body>

</html> 