<?php

if(!isset($_SESSION['username'])&& !isset($_SESSION['password'])) {
    echo 'Vous n\'êtes pas autorisé à acceder à cette zone';
    include('src/View/templates/login.html');
    exit;}

	?>
<!DOCTYPE html>
<html lang="fr">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<head>
        <title>Fiche élève</title>

    <link rel="stylesheet" type="text/css" href="src/View/css/style_fiche_eleve.css">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="src/View/images/icons/apple-touch-icon.png" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="src/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
    <!--===============================================================================================-->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!--===============================================================================================-->
</head>
<header><?php include "navbar.html.php"; ?></header>
<body>
    <div class="content">
        <div class="card">
            <!--Photo de l'eleve-->
            <div class="firstinfo">
            <img src="src/View/images/<?= $eleve->photo_forme; ?>" />
                <div class="profileinfo">
                    <h1>
                        <?= $eleve->nom_forme; ?>
                        <?= $eleve->prenom_forme; ?>
                    </h1>
                    <h3>Societe :<br>
                        <?= $eleve->nom_societe; ?><br>
                        <?= $eleve->num; ?><br>
                        <?= $eleve->rue; ?><br>
                        <?= $eleve->code_postal; ?><br>
                        <?= $eleve->ville; ?><br>
                    </h3>
                    <p class="bio">Voici la fiche demandée, pour toute modification contactez l'admin.</p>
                </div>
            </div>
        </div>
        <div class="badgescard"> <span class="devicons devicons-django"></span><span class="devicons devicons-python">
            </span><span class="devicons devicons-codepen"></span><span class="devicons devicons-javascript_badge"></span><span
                class="devicons devicons-gulp"></span><span class="devicons devicons-angular"></span><span class="devicons devicons-sass">
            </span></div>
    </div>
<footer>
    <?php include "html_foot_retour.html"; ?>
    <script></script>
</footer>
</body>

</html>

