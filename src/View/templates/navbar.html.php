
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="src/View/css/style_navbar.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>


<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">



<div class="menu"> <span></span> </div>
<nav id="nav">

    <ul class="main">
        <li><a href="?page=classes_valarep">
                <h4>Toutes les classes | Valarep</h4>
            </a></li>
        <li><a href="?page=classes_dampierre">
                <h4>Toutes les classes | Dampierre</h4>
            </a></li>
        <li><a href="?page=eleves_valarep">
                <h4>Tous les élèves | Valarep</h4>
            </a></li>
        <li><a href="?page=eleves_dampierre">
                <h4>Tous les élèves | Dampierre</h4>
            </a></li>
    </ul>

</nav>
<div class="overlay"></div>


<script>
    $('.menu, .overlay').click(function() {
        $('.menu').toggleClass('clicked');

        $('#nav').toggleClass('show');

    });
</script>
<script>
    $(document).ready(function($) {
        $(".row100").click(function() {
            window.document.location = $(this).data("href");
        });
    });
</script> 