
    <main role="main" class="container">
        <?php if ($action == "edit") : ?>
        <h1>Editer une <?= $type; ?></h1>
        <?php else : ?>
        <h1>Ajouter une <?= $type; ?></h1>
        <?php endif; ?>

        <form action="?page=<?= $type ?>-<?= $action ?>-submit" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= ($action == 'edit') ? $classe->id_classe : ''; ?>">
            <div class="form-group">
                <label for="InputNom">Nom de la classe</label><span class="text-danger">*</span>
                <input class="form-control" type="text" id="InputNom_classe" name="nom_classe" aria-describedby="RequiredField" required value="<?= ($action == 'edit') ? $classe->nom_classe : ''; ?>">
            </div>
            <div class="form-group">
                <label for="SelectStructure_id">Structure</label><span class="text-danger">*</span>
                <select class="form-control" id="SelectStructure_id" name="structure_id" aria-describedby="RequiredField" required>
                    <option <?= ($action == 'edit' && $classe->structure_id == 1) ? " selected" : ""; ?> value="1">Valarep</option>
                    <option <?= ($action == 'edit' && $classe->structure_id == 2) ? " selected" : ""; ?> value="2">Dampierre</option>

                </select>
            </div>
            <small id="RequiredField" class="form-text text-muted"><span class="text-danger">*</span> Obligatoire</small>
            <div class="form-group text-right">
                <button class="btn btn-primary" type="submit" name="btn_valider">Valider</button>
            </div>

        </form>
    </main><!-- /.container -->
    <?php include "html_foot_retour.html"; ?> 