<?php

namespace AppliValarep\Controller;


use AppliValarep\Model\Classes\Session;
use AppliValarep\Model\Dao\AuthDao;
use \Exception;

class MembreController
{
    public static function login($username, $password)
    {
        if (isset($_POST['username']) && (isset($_POST['password']))) {
            $username = $_POST["username"];
            $password = $_POST["password"];
            session_start();
            $rows = Session::getAll($username, $password);
            $membre = Session::login($username, $password);
            include "src/View/templates/landing_page/landing_page.html.php";
        } else {

            echo 'Vous n\'êtes pas autorisé à acceder à cette zone';
            include('src/View/templates/login.html');
        }
    }

    public static function deconnexion()
    {
        session_start();
        session_destroy();
        echo 'Vous êtes déconnécté';
        include('src/View/templates/login.html');
    }
}
