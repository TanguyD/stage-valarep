<?php

namespace AppliValarep\Controller;

use AppliValarep\Model\Classes\Classe;
use AppliValarep\Model\Classes\Session;
use AppliValarep\Model\Dao\AuthDao;
use AppliValarep\Model\Dao\ClasseDao;
use AppliValarep\Model\Classes\Groupe;
use AppliValarep\Model\Dao\GroupeDao;
use \Exception;

class ClasseController
{
    /**
     * Affichage de la liste des classes
     * @author Tanguy DEFER <tanguy59192@hotmail.fr>
     * @version 1.0
     */
    public static function listAction($id)
    {

        $action = "classe-membre";
        $type = "classe";
        $type_pluriel = "classes";
        $rows = Groupe::getID($id);
        include "src/View/templates/list_classe.html.php";
    }

    public static function listValarep()
    {


        $structure = "Valarep";
        $action = "valarep";
        $type = "classe";
        $type_pluriel = "classes";
        $rows = Classe::getValarep();
        include "src/View/templates/list_classe.html.php";
    }
    public static function listDampierre()
    {
        $structure = "Dampierre";
        $action = "dampierre";
        $type = "classe";
        $type_pluriel = "classes";
        $rows = Classe::getDampierre();
        include "src/View/templates/list_classe.html.php";
    }

    public static function readAction($id)
    {
        $classe = Classe::get($id);
        include "src/View/templates/list_eleves.html";
    }

    public static function addAction()
    {
        $type = "classe";
        $action = "add";
        include "src/View/templates/add_edit_classe.html.php";
    }

    public static function addSubmitAction()
    {
        global $debugMode;

        $nom_classe = filter_input(INPUT_POST, 'nom_classe', FILTER_SANITIZE_STRING);
        $regStructure_id = ["options" => ["regexp" => "/[0-2]{1}/"]];
        $structure_id = filter_input(INPUT_POST, 'structure_id', FILTER_VALIDATE_REGEXP, $regStructure_id);
        $nbRows = Classe::insert($nom_classe, $structure_id);

        if (!$debugMode || $nbRows) {
                // redirection vers la liste

                $host  = $_SERVER['HTTP_HOST'];
                $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
                $extra = '?page=classes';
                // &id=1 car seul l'admin peut accéder à cette interface
                header("Location: http://$host$uri/$extra&id=1");
            }
    }

    public static function removeAction($id)
    {
        $type = "classe";
        $classe = Classe::get($id);
        include "src/View/templates/remove_classe.html.php";
    }

    public static function removeSubmitAction($id)
    {
        global $debugMode;

        $nbRows = 1;

        if (isset($_POST['btn_yes'])) {
                // on a cliqué sur [Oui]
                // récupérer les informations de la classe à supprimer
                $classe = Classe::get($id);

                // suppression dans la base de données
                $nbRows = Classe::delete($id);
            }

        if (!$debugMode || $nbRows) {

                // redirection vers la liste
                $host  = $_SERVER['HTTP_HOST'];
                $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
                $extra = '?page=classes';
                header("Location: http://$host$uri/$extra&id=1");
            }
    }
}

