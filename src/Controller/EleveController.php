<?php

namespace AppliValarep\Controller;

use AppliValarep\Model\Classes\Eleve;
use AppliValarep\Model\Dao\EleveDao;
use AppliValarep\Model\Classes\Classe;
use AppliValarep\Model\Dao\ClasseDao;
use AppliValarep\Model\Classes\Session;
use AppliValarep\Model\Dao\AuthDao;
use \Exception;

class EleveController
{
    /**
     * Affichage de la liste des Eleves
     * @author Tanguy DEFER <tanguy59192@hotmail.fr>
     * @version 1.0
     */
    public static function listAction($id)
    {
        $id_classe = $id;
        $action = "eleve-classe";
        $type = "élève";
        $type_pluriel = "élèves";
        $rows = Eleve::getAllEleves($id);
        $classe = Classe::get($id_classe);
        include "src/View/templates/list_eleves.html.php";
    }

    public static function elevesValarep()
    {
        $structure = "Valarep";
        $action = "valarep";
        $type = "élève";
        $type_pluriel = "élèves";
        $rows = Eleve::getValarep();
        include "src/View/templates/list_eleves.html.php";
    }
    public static function elevesDampierre()
    {
        $structure = "Dampierre";
        $action = "dampierre";
        $type = "élève";
        $type_pluriel = "élèves";
        $rows = Eleve::getDampierre();
        include "src/View/templates/list_eleves.html.php";
    }
    /**
     * Affiche un eleve d'après son id
     * @param int id identifiant du eleve
     */
    public static function readAction($id)
    {

        $eleve = Eleve::get($id);
        include "src/View/templates/fiche_eleve.html.php";
    }
}

